-- 基础设置
vim.cmd([[

set number
set ts=4
set shiftwidth=4
set autoindent
set cindent

]])

-- 按键映射
require("keybindings")

-- neovim配置
require("config")

-- 加载packer插件管理器
require("plugs")

---------------------- 插件配置 -------------------------
require("plugin_config/telescope_config")
