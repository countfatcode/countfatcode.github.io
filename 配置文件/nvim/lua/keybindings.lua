function map(mode, shortcut, command)
	vim.api.nvim_set_keymap(mode, shortcut, command, {noremap=true, silent=true})
end

map("i", "jk", "<Esc>")
map("n", "H", "^")
map("n", "L", "g_")
map("n", "<c-h>", "ddkP")
map("n", "<c-l>", "ddp")


-- map("n", "ff", ":Telescope find_files")
vim.api.nvim_set_keymap("n", "<leader>ff", [[<cmd>lua require('telescope.builtin').find_files()<cr>]], {})
vim.api.nvim_set_keymap("n", "<leader>fg", [[<cmd>lua require('telescope.builtin').live_grep()<cr>]],{})
vim.api.nvim_set_keymap("n", "<leader>fb", [[<cmd>lua require('telescope.builtin').buffers()<cr>]], {})
vim.api.nvim_set_keymap("n", "<leader>fh", [[<cmd>lua require('telescope.builtin').help_tags()<cr>]], {})
vim.api.nvim_set_keymap("n", "<leader>sf", [[<cmd>lua require('telescope.builtin').file_browser()<cr>]], {})
vim.api.nvim_set_keymap("n", "<leader>/",  [[<cmd>lua require('telescope.builtin').current_buffer_fuzzy_find()<CR>]], {})
