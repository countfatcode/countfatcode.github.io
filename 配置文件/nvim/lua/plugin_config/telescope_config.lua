-- 安装所需的扩展
-- sudo apt install fd livegrep

local actions = require('telescope.actions')
require("telescope").setup{
	defaults = {
		mappings = {
			i = {
				["<C-u>"] = false
			}
		},
	},

	pickers = {
		find_files = {
			mappings = {
				n = {
					["cd"] = function(prompt_bufnr)
						local selection = require("telescope.actions.state").get_selected_entry()
						local dir = vim.fn.fnamemodify(selection.path, ":p:h")
						require("telescope.actions").close(prompt_bufnr)
						vim.cmd(string.format("silent lcd %s", dir))
					end
				}
			}
		},
	},

	extensions = {

	}
}
-- require("telescope").load_extension("fzf")
-- vim.api.nvim_set_keymap("n", "<leader>ff", [[<cmd>lua require('telescope.builtin').find_files()<cr>]], {})
-- vim.api.nvim_set_keymap("n", "<leader>fg", [[<cmd>lua require('telescope.builtin').live_grep()<cr>]],{})
-- vim.api.nvim_set_keymap("n", "<leader>fb", [[<cmd>lua require('telescope.builtin').buffers()<cr>]], {})
-- vim.api.nvim_set_keymap("n", "<leader>fh", [[<cmd>lua require('telescope.builtin').help_tags()<cr>]], {})
-- vim.api.nvim_set_keymap("n", "<leader>sf", [[<cmd>lua require('telescope.builtin').file_browser()<cr>]], {})
-- vim.api.nvim_set_keymap("n", "<leader>/",  [[<cmd>lua require('telescope.builtin').current_buffer_fuzzy_find()<CR>]], {})
